/*#############################################################
Component Name: HC06
Author: Dusty Argyle
Date Modified: April 12, 2016
Tested On: STM32F4-Discovery
OS: Windows 7
Description: Provides a library to access the USART2 module
			on the STM32F4-Discovery to establish serial
			communication with a remote device (e.g. PC)
			using the HC-06 Bluetooth module.

Requirements:
 	 	 	 	* STM32F4-Discovery Board
 	 	 	 	* HC-06 Bluetooth module

Functions:
				hc06_init
				hc06_put_char
				hc06_put_string
				hc06_clear_rx_buffer
				hc06_test
				hc06_set_baud
				hc06_set_name
				hc06_set_pin

Special Note(s) : In this driver PA2 is used as USART2_TX and
				PA3 as USART2_RX.  Any other UART and pins can
				be used, just change the relevant GPIO config-
				urations and occurrences of USART2 to the new
				UART/USART number.

##############################################################*/

#ifndef HC06_H_
#define HC06_H_
/* Includes */
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include <stdio.h>
#include <string.h>

#define HC06_RX_BUFFER_LENGTH 1	//maximum number of characters to hold in the receive buffer
#define	HC06_TIMEOUT_MAX 94000	//timeout value for waiting for a response from the HC-06 (+/- 2 secs)

void hc06_init(uint32_t speed);
void hc06_put_char(unsigned char c);
void hc06_put_string(char *str);
void hc06_clear_rx_buffer(void);
uint8_t hc06_test(void);
uint8_t hc06_set_baud(uint32_t speed);
uint8_t hc06_set_name(char *name);
uint8_t hc06_set_pin(char *pin);
void hc06_delay(uint32_t delay);
uint8_t hc06_get_command(void);
void bluetooth_init();

#endif /* HC06_H_ */


