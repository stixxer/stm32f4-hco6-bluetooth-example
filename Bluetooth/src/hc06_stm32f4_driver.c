/*#############################################################
 Component Name: HC06
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Tested On: STM32F4-Discovery
 OS: Windows 7
 Description: Provides a library to access the USART2 module
 on the STM32F4-Discovery to establish serial
 communication with a remote device (e.g. PC)
 using the HC-06 Bluetooth module.

 Requirements:
 * STM32F4-Discovery Board
 * HC-06 Bluetooth module

 Functions:
 hc06_init
 hc06_put_char
 hc06_put_string
 hc06_clear_rx_buffer
 hc06_test
 hc06_set_baud
 hc06_set_name
 hc06_set_pin

 Special Note(s) : In this driver PA2 is used as USART2_TX and
 PA3 as USART2_RX.  Any other UART and pins can
 be used, just change the relevant GPIO config-
 urations and occurrences of USART2 to the new
 UART/USART number.

 ##############################################################*/

/* Includes */
#include "hc06_stm32f4_driver.h"

/*********************************************************************************************
 * Driver variables
 *********************************************************************************************/
char HC06_rx_buffer[HC06_RX_BUFFER_LENGTH];		//used by the IRQ handler
uint8_t HC06_rx_counter = 0; 					//used by the IRQ handler
char HC06_msg[HC06_RX_BUFFER_LENGTH];//variable that contains the latest string received on the RX pin
uint8_t new_HC06_msg = 0;//flag variable to indicate if there is a new message to be serviced

/*********************************************************************************************
 Function name: UART5_IRQHandler
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Handles the interrupt when a new byte is received on the RX pin.  It works
 on the assumption that the incoming message will be terminated by a
 LF (0x10) character.
 Special Note:
 Parameters:
 Return value:
 *********************************************************************************************/
void USART2_IRQHandler(void) {
	int x;
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET) {

		/* Read one byte from the receive data register */
		HC06_rx_buffer[HC06_rx_counter] = USART_ReceiveData(USART2);

		//printf("%c", HC06_rx_buffer[HC06_rx_counter]);					//debug code

		/* if the last character received is the LF ('\r' or 0x0a) character OR if the HC06_RX_BUFFER_LENGTH (40) value has been reached ...*/
		if ((HC06_rx_counter + 1 == HC06_RX_BUFFER_LENGTH)
				|| (HC06_rx_buffer[HC06_rx_counter] == 0x0a)) {

			new_HC06_msg = 1;
			for (x = 0; x <= HC06_rx_counter; x++) //copy each character in the HC06_rx_buffer to the HC06_msg variable
				HC06_msg[x] = HC06_rx_buffer[x];

			//HC06_msg[x - 1] = '\0';				//terminate with NULL character
			memset(HC06_rx_buffer, 0, HC06_RX_BUFFER_LENGTH); //clear HC06_rx_buffer
			HC06_rx_counter = 0;

		} else {
			HC06_rx_counter++;
		}
	}
}

/*********************************************************************************************
 Function name: hc06_init
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Initializes the HC-06 Bluetooth module
 Notes :
 USARTx configured as follow:
 - BaudRate = speed parameter above
 - Word Length = 8 Bits
 - Stop Bits = 1 bit
 - No parity
 - Hardware flow control disabled (RTS and CTS signals)
 - Receive and transmit enabled
 Parameters:
 speed - 32-bit value to set the baud rate
 Return value: void
 *********************************************************************************************/
void hc06_init(uint32_t speed) {
	char buffer[20];

	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Enable USART clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	/* USART configuration */
	USART_InitStructure.USART_BaudRate = speed;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
	USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);

	/* Configure USART Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure USART Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Connect PA2 to USART2_Tx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);

	/* Connect PA3 to USART2_Rx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	/* Enable USART */
	USART_Cmd(USART2, ENABLE);

	/* Enable the UART3 Receive interrupt: this interrupt is generated when the
	 UART5 receive data register is not empty */
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	/* Enable the USART3 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Flush USART2 Tx Buffer */
	sprintf(buffer, "\n\r");
	hc06_put_string(buffer);
}

/*********************************************************************************************
 Function name   : hc06_put_char
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Writes a character to the HC-06 Bluetooth module.
 Special Notes:
 Parameters:
 ch - character to print
 Return value:
 *********************************************************************************************/
void hc06_put_char(unsigned char c) {
	/* Put character on the serial line */
	USART_SendData(USART2, (c & (uint16_t) 0x01FF));

	/* Loop until transmit data register is empty */
	while (!(USART2->SR & 0x00000040))
		;
}

/*********************************************************************************************
 Function name: hc06_put_str
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Writes a string to the HC-06 Bluetooth module.
 Special Notes:
 Parameters:
 str - string (char array) to print
 Return value:
 *********************************************************************************************/
void hc06_put_string(char *str) {
	unsigned int i = 0;
	do {
		hc06_put_char(str[i]);
		i++;
	} while (str[i] != '\0');
}

/*********************************************************************************************
 Function name: hc06_clear_rx_buffer
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Clears the software Rx buffer for the HC-06.
 Special Note:
 Parameters:
 ch - character to print
 Return value:
 *********************************************************************************************/
void hc06_clear_rx_buffer(void) {
	memset(HC06_rx_buffer, 0, HC06_RX_BUFFER_LENGTH);	//clear HC06_rx_buffer
	HC06_rx_counter = 0;						//reset the Rx buffer counter
	new_HC06_msg = 0;									//reset new message flag
}

/*********************************************************************************************
 Function name: hc06_test
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Tests if there is communications with the HC-06.
 Special Note:
 Parameters:
 Return value:
 0 -	Success
 1 -	Timeout error; not enough characters received for "OK" message
 2 -	enough characters received, but incorrect message
 *********************************************************************************************/
uint8_t hc06_test(void) {
	uint32_t timeout = HC06_TIMEOUT_MAX;
	hc06_clear_rx_buffer();	//clear rx buffer
	hc06_put_string("AT");	    //AT command for TEST COMMUNICATIONS

	//wait for "OK" - i.e. waiting for 2 characters
	while (HC06_rx_counter < 2) {
		timeout--;
		hc06_delay(1000);//wait +/- 100us just to give interrupt time to service incoming message
		if (timeout == 0)
			return 0x01;//if the timeout delay is exeeded, exit with error code
	}

	if (strcmp(HC06_rx_buffer, "OK") == 0)
		return 0x00;					//success
	else
		return 0x02;					//unknown return AT msg from HC06
}

/*********************************************************************************************
 Function name: hc06_set_baud
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Set the default Baud rate for the HC-06.
 Special Note:
 Parameters:
 speed - 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 or 230400
 Return value:
 0 - Success
 1 - Incorrect speed selected/typed
 2 -	Timeout error; not enough characters received for "OKxxxx" message
 3 -	enough characters received, but incorrect message
 *********************************************************************************************/
uint8_t hc06_set_baud(uint32_t speed) {
	uint32_t timeout = HC06_TIMEOUT_MAX;
	char buf[20];
	hc06_clear_rx_buffer();				//clear rx buffer

	//AT command for SET BAUD speed
	if (speed == 1200) {
		strcpy(buf, "OK1200");
		hc06_put_string("AT+BAUD1");
	} else if (speed == 2400) {
		strcpy(buf, "OK2400");
		hc06_put_string("AT+BAUD2");
	} else if (speed == 4800) {
		strcpy(buf, "OK4800");
		hc06_put_string("AT+BAUD3");
	} else if (speed == 9600) {
		strcpy(buf, "OK9600");
		hc06_put_string("AT+BAUD4");
	} else if (speed == 19200) {
		strcpy(buf, "OK19200");
		hc06_put_string("AT+BAUD5");
	} else if (speed == 38400) {
		strcpy(buf, "OK38400");
		hc06_put_string("AT+BAUD6");
	} else if (speed == 57600) {
		strcpy(buf, "OK57600");
		hc06_put_string("AT+BAUD7");
	} else if (speed == 115200) {
		strcpy(buf, "OK115200");
		hc06_put_string("AT+BAUD8");
	} else if (speed == 230400) {
		strcpy(buf, "OK230400");
		hc06_put_string("AT+BAUD9");
	} else {
		return 0x01;	//error - incorrect speed
	}

	//wait for "OK" message
	while (HC06_rx_counter < strlen(buf)) {
		timeout--;
		hc06_delay(1000);//wait +/- 100us just to give interrupt time to service incoming message
		if (timeout == 0)
			return 0x02;//if the timeout delay is exeeded, exit with error code
	}

	if (strcmp(HC06_rx_buffer, buf) == 0)
		return 0x00;				//success
	else
		return 0x03;				//unknown return AT msg from HC06
}

/*********************************************************************************************
 Function name: hc06_set_name
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Set the default Bluetooth name for the HC-06.
 Special Note:
 Parameters:
 name - string that represents the new name (up to 20 characters)
 Return value:
 0 - Success
 1 - error - more than 13 characters used for name
 2 - Timeout error; not enough characters received for "OKsetname" message
 3 - enough characters received, but incorrect message
 *********************************************************************************************/
uint8_t hc06_set_name(char *name) {
	uint32_t timeout = HC06_TIMEOUT_MAX;
	char buf[20];
	hc06_clear_rx_buffer();									//clear rx buffer

	if (strlen(name) > 13)				//error - name more than 20 characters
		return 0x01;

	sprintf(buf, "AT+NAME%s", name);
	hc06_put_string(buf);		//AT command for SET NAME

	//wait for "OKsetname" message, i.e. 9 chars
	while (HC06_rx_counter < 9) {
		timeout--;
		hc06_delay(1000);//wait +/- 100us just to give interrupt time to service incoming message
		if (timeout == 0)
			return 0x02;//if the timeout delay is exeeded, exit with error code
	}

	if (strcmp(HC06_rx_buffer, "OKsetname") == 0)
		return 0x00;												//success
	else
		return 0x03;						//unknown return AT msg from HC06
}

/*********************************************************************************************
 Function name: hc06_set_pin
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description:
 Special Note:
 Parameters:
 pin - string that represents the new pin number (must be 4 characters); must
 be represented by "0" - "9" characters, e.g. "1234"
 Return value:
 0 -	Success
 1 -	pin less than or more than 4 characters or/and not valid characters ("0" - "9")
 2 -	Timeout error; not enough characters received for "OKsetPIN" message
 3 -	enough characters received, but incorrect message
 *********************************************************************************************/
uint8_t hc06_set_pin(char *pin) {
	uint32_t timeout = HC06_TIMEOUT_MAX;
	char buf[20];
	hc06_clear_rx_buffer();			//clear rx buffer

	if ((strlen(pin) < 4) || (strlen(pin) > 4))
		return 0x01;				//error - too few or many characetrs in pin

	sprintf(buf, "AT+PIN%s", pin);
	hc06_put_string(buf);			//AT command for SET PIN

	//wait for "OKsetpin" message, i.e. 8 chars
	while (HC06_rx_counter < 8) {
		timeout--;
		hc06_delay(1000);//wait +/- 100us just to give interrupt time to service incoming message
		if (timeout == 0)
			return 0x02;//if the timeout delay is exeeded, exit with error code
	}
	if (strcmp(HC06_rx_buffer, "OKsetPIN") == 0)
		return 0x00;												//success
	else
		return 0x03;						//unknown return AT msg from HC06
}

/*********************************************************************************************
 Function name: hc06_delay
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Delays in us
 Special Note:
 Parameters:
 delay -	delay value in 100us increments, i.e. delay = 1 means 100us delay
 Return value:

 *********************************************************************************************/
void hc06_delay(uint32_t delay) {
	uint32_t x;
	for (x = 0; x < delay; x++) {
		//do nothing
	}
}

/*********************************************************************************************
 Function name: hc06_get_command
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Checks the message sent via bluetooth, returns 1 if start command was issued
 returns 0 if stop command was issued
 Special Note: Check this in the main loop
 Parameters:
 Return value:
 0 -	Stop
 1 -	Start
 2 -	Nothing
 *********************************************************************************************/
uint8_t hc06_get_command(void) {
	if (new_HC06_msg != 0) {
		new_HC06_msg = 0;
		if (HC06_msg[0] == 's') {		//s is 115 0x
			return 0x01;
		} else if (HC06_msg[0] == 'q') {  // q is 113
			return 0x00;
		}
	}

	return 0x02;
}

/*********************************************************************************************
 Function name: bluetooth_start
 Author: Dusty Argyle
 Date Modified: April 12, 2016
 Description: Call this to start up bluetooth with the default settings
 Special Note:
 Parameters:
 Return value:
 *********************************************************************************************/
void bluetooth_init() {
	//SYStick interrupt
	RCC_ClocksTypeDef RCC_Clocks;

	uint8_t count = 0;
	char buffer[40];
	uint8_t rcv;

	/* Set the SysTick Interrupt to occur every 1ms) */
	RCC_GetClocksFreq(&RCC_Clocks);
	if (SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000))
		while (1)
			;//will end up in this infinite loop if there was an error with Systick_Config

	hc06_init(9600);
	printf("\n\nDebug messages on SWV:\n");
	hc06_delay(1000);

	//Test communications with HC-06. rcv = 0 if ok
	printf("Test Communications...");
	rcv = hc06_test();
	if (rcv == 0)
		printf("OK\n");
	else
		printf("Err %d\n", rcv);
}

